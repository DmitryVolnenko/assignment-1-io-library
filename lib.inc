%define MINUS 0x2D
%define EXIT 60
%define NEW_STRING 0xA
%define NULL_TERMINATOR 0
%define SYS_CALL 1
%define STDIN 0
%define STDOUT 1
%define SPACE 0x20
%define TAB 0x9
%define MIN 0x30
%define MAX 0x39

section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, EXIT
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
.lop:
    cmp byte [rdi+rax], NULL_TERMINATOR
    je .rt
    inc rax
    jmp .lop
.rt:
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
	push rax
	push rsi
	push rdx


    mov rsi, rdi
    call string_length
    mov rdx, rax
    mov rax, SYS_CALL
    mov rdi, STDOUT
    syscall

    pop rdx
	pop rsi
	pop rax
	pop rdi
    
    ret


; Принимает код символа и выводит его в stdout
print_char:
    dec rsp
    mov [rsp], dil
    mov rax, SYS_CALL
    mov rdi, STDOUT
    mov rsi, rsp
    mov rdx, 1
    syscall
    inc rsp
    ret


; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, NEW_STRING
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    push r12 ; callee-saved
    push r9
    mov r9, rsp
    mov byte[rsp], NULL_TERMINATOR ; terminator
    mov rax, rdi
    mov r12, 10 ; /10 -> 10 CC
    .loop:
        xor rdx, rdx ; important for the correctness of the result
        div r12
        add rdx, '0' ;MOD(/)+0 -> ASCII [0-9](30-39)
        dec rsp
        mov byte[rsp], dl
        cmp rax, 0
        je .end
        jmp .loop		        
    .end:
	mov rdi, rsp
    	call print_string
	mov rsp, r9
	pop r9
	pop r12
	ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    cmp rdi, 0
    jns .ns ; если число положительное сразу выводим его
    .print_snumber:
        push rdi
        mov rdi, MINUS; minus-sign
        call print_char ; 
        pop rdi
        neg rdi ;
    .ns:
        call print_uint ; print ns-number
        ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    .loop:
        mov al, byte [rdi] ; перемещаем один символ первой строки в аккумулятор
	cmp al, byte [rsi] ; сравниваем с символом второй строки
	jne .ne 	   ; если не равны то выходим с 0 в аккумуляторе
	inc rdi		   ; переводим указатели на следующий символ
	inc rsi
	cmp al, NULL_TERMINATOR          ; проверка на конец строки
	jne .loop          ; если строка не кончилась, повторяем
    .equals:
        mov rax, SYS_CALL
	ret
    .ne:
        xor rax, rax
	ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    dec rsp
    mov [rsp], byte 0
    mov rax, STDIN
    mov rdi, STDIN
    mov rsi, rsp
    mov rdx, 1
    syscall
    mov al, [rsp]
    inc rsp
    ret


; Принимает: адрес начала буфера, размер буфера
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
    push r12
    push r13
    push r14
    mov r12, rdi
    mov r13, rsi
    dec r13
    xor r14, r14
    .space_symb:
        call read_char
        cmp rax, SPACE
        je .space_symb
        cmp rax, TAB
        je .space_symb
        cmp rax, NEW_STRING
        je .space_symb
        cmp rax, 0
        je .end
								            
    .read:
   	cmp r13, r14
   	je .err
        mov byte[r12+r14], al
        inc r14
        call read_char
        cmp rax, 0
        je .end
        cmp rax, SPACE
        je .end
        cmp rax, TAB
        je .end
        cmp rax, NEW_STRING
        je .end
        jmp .read
								        
   .err:
       	xor rax, rax	
        pop r14
        pop r13
        pop r12
        ret
     
    .end:
	mov byte[r12+r14], 0
        mov rax, r12
	mov rdx, r14
        pop r14
        pop r13
        pop r12
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rsi, rsi
    xor rax, rax

    push rbx
    mov rbx, 10;
.lp:
    
    xor r10, r10
    mov r10b, byte [rdi+rsi]
    cmp r10, MIN
    jl .rt
    cmp r10, MAX
    jg .rt
    mul rbx
    add rax, r10
    sub rax, MIN
    inc rsi
    jmp .lp
.rt:
    pop rbx
    mov rdx, rsi
    ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
      xor rax, rax
    push rdi
    call read_char
    pop rdi
    cmp al, MINUS
    jz .neg
    jmp parse_uint
   .neg: 
        inc rdi
        call parse_uint
        neg rax
        inc rdx
        ret 


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax

	push rdi
	push rsi
	push rdx
	call string_length
	pop rdx
	pop rsi
	pop rdi

    cmp rax, rdx
    ja .notgood
    .loop:
        mov r10b, byte [rdi]
        mov byte [rsi], r10b
        inc rdi
        inc rsi
	test r10b, r10b
        jz .good
        jmp .loop
    .notgood:
        mov rax, 0
        ret
    .good:
        ret
